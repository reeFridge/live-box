FROM node:12.14.0-stretch

RUN apt-get update && apt-get install -yq --no-install-recommends default-jdk

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY package*.json ./

USER node

RUN npm install

COPY --chown=node:node . .

RUN npm run build.deps && tar -C ./webos -xzf ./webos/cli.tar.gz

EXPOSE 1337

ENTRYPOINT ["npm", "run"]
CMD ["start"]
