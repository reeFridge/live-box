# live-box

Dockerized smart-tv app powered by `zombiebox`, `virtual-live` and inspired by 'IPv6 Christmas Tree' that can play live streams and many more cool things.

## Platforms

* Webos (tested on fresh (e.g. 2019, 2018) models)

## Docker

You can build and even develop applicaiton inside docker without needing install dependencies on local machine.

### Build image

```
docker build -t ifaced/live-box .
```

### Start zombiebox dev-server

```
docker run -it --rm -p 1337:1337 -v <absolute path to project root>/src:/home/node/app/src fridge/live-box
```

### Build zombiebox application

```
docker run -it --rm -v <absolute path to project root>/dist:/home/node/app/dist -v <absolute path to project root>/src:/home/node/app/src fridge/live-box 'build'
```
