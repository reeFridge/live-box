import Scene from 'zb/layers/scene';
import Rect from 'zb/geometry/rect';
import MarqueeWithData from '../../widgets/marquee-with-data/marquee-with-data';

const Streams = {
	ChristmasTrees: 'http://109.194.35.160/api/streams/live/operators/70b646ca-4215-4fe6-bdef-46d76fa4e194/channels/ef3c4dc5-9155-4e01-bdb8-1e20e4f7afda/master.m3u8',
	Test: 'http://109.194.35.160/test/live/master.m3u8'
};


/**
 */
export default class Home extends Scene {
	constructor(video) {
		super();

		this.video = video;

		this._marqueeWithData = new MarqueeWithData();

		this._container.appendChild(this.render());
	}

	afterDOMShow() {
		super.afterDOMShow();

		this._marqueeWithData
			.preload()
			.then(() => this._marqueeWithData.start());
	}

	render() {
		this.video.prepare(Streams.ChristmasTrees);

		this.video.on(this.video.EVENT_READY, () => {
			this.video.play();
		});

		const viewport = this.video.getViewport();
		viewport.setArea(new Rect({x0: 0, y0: 0, x1: 1920, y1: 1080}));

		return h('div#ui.container', {style: { 'width': '1920px', 'height': '1080px' }},
			h('div#label.container', {
				style: {
					'background-color': 'rgba(0, 0, 255, 0.5)',
					'padding': '20px',
					'font-size': '25px',
					'width': '90px',
				}
			},
				h('div', 'live-box')
			),
			this._marqueeWithData.getContainer(),
		);
	}
}
