import SpeedMarquee from 'ui/widgets/marquee/speed-marquee';

const createArea = () => {
	return h('div', {
		style: {
			'height': '60px',
			'background-color': 'rgba(0, 0, 0, 0.7)',
			'overflow': 'hidden',
			'position': 'absolute',
			'right': '0px',
			'bottom': '0px',
			'left': '0px',
		}
	});
};

const createMessage = () => {
	return h('div', {
		style: {
			'font-size': '36px',
			'line-height': '60px',
			'color': '#fff',
			'white-space': 'nowrap',
		}
	});
};

const createShift = () => {
	return h('div', {
		style: {
			'width': '1920px',
			'height': '60px',
		}
	});
};

const createSlide = (message, shiftLeft, shiftRight) => {
	return h('div', {
		style: {
			'display': 'flex',
			'position': 'absolute',
			'top': '0px',
			'left': '0px',
		}
	}, shiftLeft, message, shiftRight);
};

/**
 */
export default class Marquee extends SpeedMarquee {
	/**
	 */
	constructor() {
		const area = createArea();
		const message = createMessage();
		const shiftLeft = createShift();
		const shiftRight = createShift();
		const slide = createSlide(message, shiftLeft, shiftRight);

		area.appendChild(slide);

		super({
			container: area,
			slider: slide,
			speed: 0.3,
		});

		this._message = message;
	}

	/**
	 * @return {HTMLElement}
	 */
	getContainer() {
		return this._container;
	}

	/**
	 * @param {string} message
	 */
	setMessage(message) {
		this._message.innerText = message;
	}

	/**
	 * @override
	 */
	_changeDirection() {
		this._fireEvent(this.EVENT_SLIDE_ENDED);
	}
}

/**
 * Fired with: nothing
 * @const {string}
 */
Marquee.prototype.EVENT_SLIDE_ENDED = 'slide-ended';
