import EventPublisher from 'zb/events/event-publisher';
import DataList from 'ui/data/list';
import Marquee from '../marquee/marquee';
import MessagesApi, {Event as MessageEvent, MessageType} from '../../api/messages';


/**
 */
export default class MarqueeWithData extends EventPublisher {
	/**
	 */
	constructor() {
		super();

		this._messagesApi = new MessagesApi({host: '192.168.7.56'});

		/**
		 * @type {DataList}
		 * @private
		 */
		this._messageList = new DataList();

		/**
		 * @type {Marquee}
		 * @private
		 */
		this._marquee = new Marquee();
		this._marquee.on(this._marquee.EVENT_SLIDE_ENDED, () => this._onSlideEnded());

		this._messagesApi.on(MessageEvent.EVENT_NEW, (event, message) => {
			this._messageList.add(message);
		});
	}

	/**
	 * @return {Promise<void>}
	 */
	preload() {
		return this._messagesApi.getAll()
			.then((messages) => {
				this._messageList.addItems(messages);
				const {message} = this._messageList.current();
				this._marquee.setMessage(message);
			});
	}

	/**
	 */
	start() {
		if (this._messageList.size()) {
			this._marquee.start();
		}
	}

	/**
	 * @return {HTMLElement}
	 */
	getContainer() {
		return this._marquee.getContainer();
	}

	/**
	 *
	 * @param {number} min
	 * @param {number} max
	 * @return {number}
	 * @private
	 */
	_getRandomNumber(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	/**
	 * @return {string}
	 * @private
	 */
	_getNextItem() {
		const max = this._messageList.size() - 1;

		if (this._messageList.currentIndex() < max) {
			this._messageList.selectNextItem();
			return this._messageList.current();
		} else {
			const random = this._getRandomNumber(0, max);
			return this._messageList.itemAt(random);
		}
	}

	/**
	 * @private
	 */
	_onSlideEnded() {
		this._marquee.stop();

		if (this._messageList.size()) {
			const {message} = /** @type {MessageType} */(this._getNextItem());
			this._marquee.setMessage(message);
			this._marquee.start();
		}
	}
}
