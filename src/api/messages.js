import EventPublisher from 'zb/events/event-publisher';
import {send} from 'zb/http/xhr';

/**
 * @typedef {{message: string, id: string}}
 */
export let MessageType;

export const Event = {
	EVENT_NEW: 'new-message'
};

function deserializeMessage(raw) {
	return {
		message: raw['message'],
		id: raw['id']
	};
}

export default class extends EventPublisher {
	constructor({host}) {
		super();

		this.base = `http://${host}/api`;
		this.host = host;
		this.subscribeOnUpdates();
	}

	subscribeOnUpdates() {
		const url = new URL(`http://${this.host}:8181/.well-known/mercure`);
		url.searchParams.append('topic', `${this.base}/new_year_messages/{id}/outdated`);
		const eventSource = new EventSource(url.toString());
		eventSource.onmessage = ({data}) => {
			this._fireEvent(Event.EVENT_NEW, deserializeMessage(JSON.parse(data)));
		};
	}

	/**
	 * @return {Promise}
	 */
	getAll(format = 'json') {
		return send(`${this.base}/new_year_messages`, {headers: {'Accept': `application/${format}`}})
			.then((xhr) => {
				return JSON.parse(xhr.response.toString())
					.map(deserializeMessage);
			});
	}
}
