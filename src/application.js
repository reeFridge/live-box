import BaseApplication from 'generated/base-application';
import Home from './scenes/home/home';

const Scene = {
	HOME: 'home'
};

export default class Application extends BaseApplication {
	constructor() {
		super();
	}

	home() {
		const layerManager = this.getLayerManager();
		const home = layerManager.getLayer(Scene.HOME);
		this.showVideo();
		return layerManager.open(home);
	}

	onReady() {
		const video = this.device.createStatefulVideo();
		this.addScene(new Home(video), Scene.HOME);
		this.home();
	}
}
