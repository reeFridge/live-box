const path = require('path');

module.exports = () => ({
	project: {
		name: 'live-box',
		src: srcPath(),
		entry: srcPath('application.js')
	},
	include: [{
			name: 'hyperscript',
			inlineScripts: [rootPath('deps', 'hyperscript', 'index.js')],
			externs: [rootPath('externs', 'hyperscript.js')]
	}],
	define: {
		ENVIRONMENT: '',
	},
	platforms: {
		tizen: {
			widget: platformsPath('tizen', 'config.xml'),
			securityProfile: 'live-box'
		},
		webos: {
			toolsDir: rootPath('webos', 'cli', 'bin'),
			appinfo: {
				title: 'live-box',
				id: 'com.zombiebox.livebox',
				type: 'web',
				main: 'index.html'
			}
		}
	}
});

function rootPath(...args) { return path.join(__dirname, ...args); }
function srcPath(...args) { return path.join(__dirname, 'src', ...args); }
function platformsPath(...args) { return path.join(__dirname, 'platforms', ...args); }
